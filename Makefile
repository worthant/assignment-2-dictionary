ASM = nasm
ASM_FLAGS = -g -f elf64

SRC_DIR := src
BIN_DIR := bin

EXEC := $(BIN_DIR)/main
OBJS := $(addprefix $(BIN_DIR)/, lib.o dict.o main.o)
ERR_FILE := $(BIN_DIR)/err.txt

all: $(BIN_DIR) $(EXEC)

$(BIN_DIR):
	mkdir -p $(BIN_DIR)

$(BIN_DIR)/%.o: $(SRC_DIR)/%.asm
	$(ASM) $(ASM_FLAGS) -I$(SRC_DIR)/ -o $@ $<

$(EXEC): $(OBJS)
	ld -o $@ $^

test:
	@bash run_tests.sh

clean:
	rm -rf $(BIN_DIR)

.PHONY: all test clean
