#!/bin/bash

string_to_repeat="a"
length_required=257  # for buffer overflow
long_string=$(printf "%${length_required}s" | tr ' ' "${string_to_repeat}")

# Basic tests
declare -a basic_tests=("third" "second" "nonexistent_key" "$long_string")
declare -a basic_expected=("value 3" "value 2" "Key wasn't found!" "Key should be < 256 chars!")

# Edge cases and special scenarios
declare -a edge_tests=(
    ""                  # Empty string
    "     "             # Only spaces
    "SeConD"            # Mixed case
    " second "          # Spaces at the start and end
    "!@#$%^&*()"        # Special characters
    "second123"         # Numbers in the key
    "$(printf 'a%.0s' {1..256})"   # Very long but valid
    "$(printf 'a%.0s' {1..257})"   # Exact limit (255 + null-term)
    "secondEXTRA"       # Starts with valid key
    "second third"      # Multiple words
)

declare -a edge_expected=(
    "Key wasn't found!"
    "Key wasn't found!"
    "Key wasn't found!"
    "value 2"
    "Key wasn't found!"
    "Key wasn't found!"
    "Key wasn't found!"
    "Key should be < 256 chars!"
    "Key wasn't found!"
    "value 2"
)

# Combine all the tests and expected results
tests=("${basic_tests[@]}" "${edge_tests[@]}")
expected=("${basic_expected[@]}" "${edge_expected[@]}")

# Execute the tests
for i in "${!tests[@]}"; do
    output=$(echo -n "${tests[$i]}" | ./bin/main 2>&1)

    if [ "$output" == "${expected[$i]}" ]; then
        echo "Test $i passed."
    else
        echo "Test $i failed. Expected: ${expected[$i]}. Got: $output"
    fi
done
