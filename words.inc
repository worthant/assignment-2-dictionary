%include "colon.inc"
%define START first

section .rodata
; just create some words here, created by colon macro
; this section is read-only, because we will use those words like a constants
; (rodata == ReadOnly data)

colon "third", third
db "value 3", 0

colon "second", second
db "value 2", 0

colon "first", first
db "value 1", 0

