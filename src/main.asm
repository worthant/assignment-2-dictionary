%include "colon.inc"
%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define BUFFER_SIZE 256

section .bss
; It is required to read a string with a size <= 255 chars + 1 for null terminator in stdin
input_buffer: resb BUFFER_SIZE

section .rodata
error_message: db "Key wasn't found!", 0
length_error:  db "Key should be < 256 chars!", 0

section .text
global _start

_start:
    ; Read a word from stdin into input_buffer
    mov rdi, input_buffer
    mov rsi, BUFFER_SIZE
    call read_word

    test rax, rax
    jz .input_too_long

    ; Find the word in the dictionary
    mov rdi, rax       ; Find this word
    mov rsi, START     ; Dictionary start
    call find_word

    test rax, rax      ; If the word is not found
    jz .not_found      ; -> find_word returns 0

    ; Retrieve the value associated with the key
    mov rdi, rax              ; Address of dictionary key
    call retrieve_value_by_key_address
    mov rdi, rax              ; Address of corresponding value
    mov rsi, 1                ; file descriptor for stdout
    call print_to_descriptor  ; Print value
    call print_newline
    jmp .exit

.input_too_long:
    ; If too long -> write error msg to stderr
    mov rdi, length_error
    jmp .err
.not_found:
    ; If not found -> write error msg to stderr
    mov rdi, error_message
.err:
    mov rsi, 2  ; file descriptor for stderr
    call print_to_descriptor

.exit:
    ; Exit with 0 as the return code
    xor rdi, rdi
    call exit
