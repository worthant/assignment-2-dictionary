global exit
global string_length
global string_copy
global string_equals
global print_char
global print_newline
global print_uint
global print_int
global read_char
global read_word
global parse_uint
global parse_int
global print_to_descriptor

; Define magic numbers
%define DECIMAL_BASE 10  ; base of decimal numbers
%define ASCII_ZERO '0'   ; code for 0 in ASCII
%define MAX_DIGITS 20    ; maximum number of digits in uint64
%define ASCII_MINUS '-'
%define WHITESPACE ' '
%define TAB `\t`
%define NEXTLINE `\n`
%define ASCII_PLUS '+'

; Macro to skip whitespace characters.
; Whitespace includes: space (0x20), tab (0x9), and newline (0xA).
%macro JMP_IF_WHITESPACE 2
	cmp	%1, ' '
	je	%2
	cmp	%1, 0x9
	je	%2
	cmp	%1, 0xA
	je	%2
%endmacro

; Macro to save the callee-saved registers.
%macro SAVE 1-*
%rep %0
	push	%1
%rotate 1
%endrep
%endmacro

; Macro to restore the callee-saved registers.
%macro RESTORE 1-*
%rep %0
%rotate -1
	pop	%1
%endrep
%endmacro

section .text
 
; Takes return code (from rdi) and ends current process
exit:
    mov rax, 60
    syscall

; Takes ptr to zero-terminated string (from rdi), returns its length (rax)
string_length:
    xor rax, rax

    .loop:
    	cmp byte[rdi + rax], 0
	jz .end
	inc rax
	jmp .loop

    .end:
    	ret

; Takes symbol code (from rdi) and outputs it to stdout
print_char:
    xor rax, rax
    
    push di       ; stack should be 2, 4 and 8 - bytes, cant be 1 (dil) 
    mov rax, 1    ; "syscall" number for "sys_write"
    mov rdi, 1    ; file descriptor for "stdout"
    mov rsi, rsp  ; char is stored here
    mov rdx, 1    ; number of bytes to write
    syscall
    pop di        ; restore stack pointer
    ret

; Prints newline (outputs 0xA symbol)
print_newline:
    mov di, NEXTLINE
    jmp print_char ; Tail call optimization

; Outputs unsigned 8-byte number in decimal format
; Note: allocate some space in stack and store division results there
; Dont forget about converting numbers to their ASCII codes
print_uint:
    mov r8, MAX_DIGITS
    sub rsp, MAX_DIGITS + 12
    mov byte [rsp + r8], 0
    mov rax, rdi
    mov r9, DECIMAL_BASE

    .loop_divide:
        xor rdx, rdx
	div r9
	dec r8
	add dl, ASCII_ZERO
	mov [rsp + r8], dl
	test rax, rax
	jz .end_loop
	jmp .loop_divide

    .end_loop:
        lea rdi, [rsp + r8]
        mov rsi, 1  ; file descriptor for stdout
        call print_to_descriptor
        add rsp, MAX_DIGITS + 12
        ret

; Prints signed 8-byte number in decimal format
print_int:
    test rdi, rdi
    jns print_uint  ; if number isnt negative, jump to unsigned handler
    
    neg rdi         ; if negative -> just revert it
    push rdi
    mov dil, ASCII_MINUS
    call print_char ; then, print minus
    pop rdi
    jmp print_uint  ; now, just print positive number

; Takes two ptrs to zero-terminated strings (from rdi)
; Returs: 1, if they are equals (to rax)
; Else: 0
string_equals:
    xor r8, r8

    .loop:
	movzx rax, byte [rdi +r8]
	cmp al, byte [rsi + r8]  ; compare two bytes
	jne .not_eq

	test rax, rax             ; checking for null terminator
	jz .eq

	inc r8                    ; increment ptr offset
        jmp .loop
    
    .eq:
	mov rax, 1
	ret
    .not_eq:
	xor rax, rax
	ret

; Reads one symbol from stdin and returns it.
; Returns 0 if we have reached the end of stream
read_char:
    xor rax, rax  ; syscall for read
    sub rsp, 16   ; allocate space in stack
    xor rdi, rdi  ; 0 for stdin file descriptor
    mov rsi, rsp  ; addr of stack space
    xor rdx, rdx  ; count
    inc rdx	  ; number of bytes to read
    syscall
	
    test rax, rax
    jz .end

    xor	rax, rax
    mov	al, [rsp] ; char here

    .end:
	add rsp, 16
	ret

; Takes: address of the start from the buffer (rdi), buffer size (rsi)
; Reads: a word from stdin to buffer, skipping whitespace characters in the start, .
; Whitespace chars are: whitespace 0x20, tab 0x9 and newline 0xA
; Stops and returns 0 if the word is too big for the buffer
; If success -> returns:
; - buffer addr (to rax)
; - word length (to rdx)
; If not success -> return 0 (to rax)
; This function should add null-terminator to the word
read_word:
    SAVE rbx, r14, r15 ; saving callee-saved regs
    mov	r14, rdi       ; buffer addr to r14
    xor	r15, r15       ; r15 is length counter
    mov	rbx, rsi       ; rbx is buffer length

    .ws_chars:
	call read_char
	JMP_IF_WHITESPACE al, .ws_chars

    .loop:
	test al, al
	jz .ok          ; end of the word
	cmp rbx, r15
	jbe .fail       ; failing if no place for null-terminator

	mov byte [r14 + r15], al

	call read_char
	inc r15         ; incrementing length counter

	JMP_IF_WHITESPACE al, .ok
	jmp .loop

    .ok:
	mov byte [r14 + r15], 0 ; adding null-terminator
	mov rax, r14
	mov rdx, r15
	jmp .end

    .fail:
	xor rax, rax            ; 0 in rax indicates failure

    .end:
	RESTORE	rbx, r14, r15   ; restoring callee-saved regs
	ret

; Takes ptr to string (from rdi)
; Tries to read unsigned number from its start
; Returns (to rax): number
; Returns (to rdx): its length in symbols
; rdx = 0 if the number failed to read
parse_uint:
    xor rax, rax    ; number will be here
    xor rdx, rdx    ; number's length will be here 

    test rdi, rdi   ; check if the ptr is null
    jz .end

.loop:
	movzx rcx, byte [rdi] ; load char from the string
	sub cl, ASCII_ZERO    ; convert ASCII char to its integer value
	cmp cl, 9             ; check if its a digit from 0 to 9
	ja .end               ; jump if its >= 9 (jump if Above => cf and zf == 0) 
	
	imul rax, rax, DECIMAL_BASE ; shifting digits to the left by 1 position, making room for a new digit
	add rax, rcx                ; add a new digit to rax
	
	inc rdi ; increment ptr to current position in the string
	inc rdx ; increment number length
	
	jmp .loop

.end:
	ret

; Takes ptr to strind (rdi)
; Tries to read signed number from its start
; Spaces in signed number between sign and number are not allowed
; Returs (to rax): number
; Returns (to rdx): its length in symbols (including sign, if it exists)
; rdx = 0 if number failed to read
parse_int:
    xor rax, rax    ; number will be here
    xor rdx, rdx    ; number length will be here
    xor rcx, rcx    ; temporary register for sign and ASCII conversion

    test rdi, rdi   ; testing if ptr is not null
    jz .end

    movzx rcx, byte [rdi] ; loading the first char

    cmp rcx, ASCII_MINUS  ; check for negative sign
    je .found_negative

    cmp rcx, ASCII_PLUS   ; check for positive sign
    je .found_positive

    jmp parse_uint

.found_negative:
	mov rcx, -1
	inc rdi
	inc rdx
	call parse_uint
	imul rax, rcx
	inc rdx
	jmp .end

.found_positive:
	inc rdi
	inc rdx
	jmp parse_uint
    
    .end:
	ret

; Takes ptr to string(rdi), ptr to buffer(rsi) and buffer length(rdx)
; Copies string to buffer
; Returns string length (rax) if it fits in the buffer
; If its not -> returns 0 (rax)
string_copy:
    xor rax, rax ; holds copied string length
    
.loop:
	mov cl, byte [rdi + rax]
	cmp rax, rdx
	jae .buffer_full          ; jump if the buffer is sMoL

	mov byte [rsi + rax], cl  ; copy byte to destination
	inc rax                   ; increment length
	
	test cl, cl
	jnz .loop

.done:
	mov byte [rsi + rax], 0   ; Add null-terminator to string
	ret

.buffer_full:
	xor rax, rax
	ret

; Takes ptr to zero-terminated string (from rdi) and prints it to the given file descriptor
; Input: rdi = ptr of null-terminated str, rsi = file descriptor
; Output: the string itself, to the given file descriptor
print_to_descriptor:
    push rsi
    push rdi
    call string_length
    pop rsi      ; Restore ptr to rdi (where does the string start?)
    pop rdi      ; File descriptor for std in/out/err here
    mov rdx, rax ; rdx stores data during i/o -> string length (how many bytes to write?)
    mov rax, 1   ; "syscall" number for "sys_write"
    syscall
    ret
