%include "lib.inc"
global find_word
global retrieve_value_by_key_address

section .text

; Takes 2 args
; arg1: prt to null-terminated-str (in rdi)
; arg2: prt to dict start (in rsi)
; if string found in dict -> returns entry addr
; if not -> returns 0
find_word:
    ; Save callee-saved registers
    push r14
    push r15
    mov r14, rdi ; ptr to str here
    mov r15, rsi ; ptr to dictionary beginning here

.loop:
    ; If r15 (current node) is null, we've reached the end
    test r15, r15
    jz .not_found

    ; Compare the strings
    mov rdi, r14             ; string1: search string
    lea rsi, [r15 + 8]       ; string2: current node's key (skip the 8-byte next pointer)
    call string_equals       ; Use string_equals function

    test eax, eax
    jnz .found               ; If not zero, the strings match

    ; Move to the next node
    mov r15, [r15]

    ; Continue loop
    jmp .loop

.found:
    mov rax, r15   ; Set return value to the node's address
    jmp .end

.not_found:
    xor rax, rax   ; Return 0

.end:
    pop r15        ; Restore callee-saved registers
    pop r14
    ret

; Given the address of a dictionary entry (key), retrieves the address of the value.
; Input: rdi = address of dictionary entry
; Output: rax = address of corresponding value
retrieve_value_by_key_address:
    lea rdi, [rdi+8]    ; to get the value we need to skip 8 bytes
    push rdi            ; save caller-saved register, because could be modified in string_length
    call string_length  ; Count the number of chars in the key string (returned to rax)
    pop rdi             ; restore rdi
    add rdi, rax        ; Move past the length of the key string
    inc rdi             ; Move past the null-terminator to get to the value address
    mov rax, rdi        ; Return the value address in rax
    ret
