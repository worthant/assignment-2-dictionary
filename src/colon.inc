; macro for creating linked dictionary-list
%macro colon 2      ; 2 args - (key, value label)
	align 8         ; align to 8-byte boundary (good for performance)

	%2:             ; create (%2) label ((colon "key", value) -> (value) label)
		%ifdef head ; if head is defined -> set next ptr to head
		    dq head
		%else       ; if not defined -> set next ptr to 0
		    dq 0
        %endif

        db %1, 0    ; store the key string and null-terminate it

    %define head %2 ; redefine head to the label of the current node
%endmacro

